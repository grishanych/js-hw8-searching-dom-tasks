const getAllParagraphs =  document.querySelectorAll('p');

getAllParagraphs.forEach(function(paragraph) { 
    paragraph.style.backgroundColor = '#ff0000';
});


const optionsList = document.getElementById('optionsList');
console.log(optionsList)

const parentElement = optionsList.parentNode;
console.log(parentElement)


//--------------------------------////
const childNodes = optionsList.childNodes;
console.log(childNodes)

childNodes.forEach(function(node) { 
    if (node.nodeType === Node.ELEMENT_NODE) {
      console.log(node.nodeName + ' (Element node)');
    } else if (node.nodeType === Node.TEXT_NODE) {
      console.log('Text node');
    }
  });

  //--------------------------------////

let newParagraps = document.getElementById('testParagraph');
newParagraps.textContent ='This is a paragraph'


let elemFromMainHeader = document.querySelector('.main-header');
console.log(elemFromMainHeader);

const nestedElements = elemFromMainHeader.querySelectorAll('*'); 
nestedElements.forEach(function(element) { 
  element.classList.add('nav-item'); 
  console.log(element); 
});


let elemSectionTitle = document.querySelectorAll('.section-title');
console.log(elemSectionTitle);
elemSectionTitle.forEach(function(element) {
    element.classList.remove('section-title');
  });
  console.log(elemSectionTitle);